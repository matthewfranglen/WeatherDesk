#!/usr/bin/env python3
# coding: utf-8

# Copyright © 2016 Bharadwaj Raju <bharadwaj.raju777@gmail.com>
# All Rights Reserved.

# Original code taken from the following answers by StackOverflow user
# Martin Hansen (http://stackoverflow.com/users/2118300/martin-hansen):
# - http://stackoverflow.com/a/21213358/5413945
# - http://stackoverflow.com/a/21213504/5413945

# This file is part of WeatherDesk.
#
# WeatherDesk is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# WeatherDesk is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with WeatherDesk (in the LICENSE file).
# If not, see <http://www.gnu.org/licenses/>.

from pathlib import Path

TARGET = Path.home() / ".local" / "share" / "backgrounds" / "current.jpg"

def set_wallpaper(image):
    data = Path(image).read_bytes()
    TARGET.write_bytes(data)
